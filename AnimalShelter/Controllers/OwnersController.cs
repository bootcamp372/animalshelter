﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AnimalShelter.Models;
using AnimalShelter.Repository;

namespace AnimalShelter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnersController : ControllerBase
    {
        private IOwnerRepository _ownerRepository;
        private readonly AnimalShelterContext _context;

        public OwnersController(
            AnimalShelterContext context
            , IOwnerRepository ownerRepository
            )
        {
            _context = context;
            _ownerRepository = ownerRepository;
        }

            // GET: api/Owners
            [HttpGet]
        public async Task<ActionResult<IEnumerable<Owner>>> GetOwners()
        {
            if (_context.Owners == null)
            {
                return NotFound();
            }
            IEnumerable<Owner> owners = _ownerRepository.GetAll();
            if (owners == null)
            {
                return NotFound();
            }
            else
            {
                return owners.ToList();
            }
        }

        // GET: api/Owners/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Owner>> GetOwner(int id)
        {
            Owner owner = _ownerRepository.GetById(id);
            if (owner == null)
            {
                return NotFound();
            }
            else
            {
                return owner;
            }
        }

        // PUT: api/Owners/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOwner(int id, Owner owner)
        {
            if (id != owner.OwnerId)
            {
                return BadRequest();
            }

            // Update existing owner entry
            try
            {
                _ownerRepository.Update(owner);
                _ownerRepository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_ownerRepository.OwnerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


            return NoContent();
        }

        // POST: api/Owners
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Owner>> PostOwner(Owner owner)
        {
            if (_context.Owners == null)
            {
                return Problem("Entity set 'AnimalShelterContext.Animals'  is null.");
            }


            _ownerRepository.Insert(owner);
            _ownerRepository.Save();

            return CreatedAtAction("GetOwner", new { id = owner.OwnerId }, owner);
        }

        // DELETE: api/Owners/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(int id)
        {
            if (_context.Owners == null || !_ownerRepository.OwnerExists(id))
            {
                return NotFound();
            }

            _ownerRepository.Delete(id);
            _ownerRepository.Save();

            return NoContent();
        }

        private bool OwnerExists(int id)
        {
            return (_context.Owners?.Any(e => e.OwnerId == id)).GetValueOrDefault();
        }
    }
}
