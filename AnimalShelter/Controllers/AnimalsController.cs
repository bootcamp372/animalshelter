﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AnimalShelter.Models;
using AnimalShelter.Repository;

namespace AnimalShelter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalsController : ControllerBase
    {
        private IAnimalRepository _animalRepository;
        private readonly AnimalShelterContext _context;

        public AnimalsController(
            AnimalShelterContext context
            , IAnimalRepository animalRepository
            )
        {
/*            _animalRepository = new AnimalRepository(new AnimalShelterContext());*/
            _context = context;
            _animalRepository = animalRepository;
        }

        // GET: api/Animals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Animal>>> GetAnimals()
        {
            if (_context.Animals == null)
            {
                return NotFound();
            }
            IEnumerable<Animal> animals = _animalRepository.GetAll();
            if (animals == null)
            {
                return NotFound();
            }
            else
            {
                return animals.ToList();
            }

        }

        // GET: api/Animals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Animal>> GetAnimal(int id)
        {
            Animal animal = _animalRepository.GetById(id);
            if (animal == null)
            {
                return NotFound();
            }
            else
            {
                return animal;
            }
        }

        // GET: api/Animals/owner/5
        // returns a list of animals based on owner ID
        [HttpGet("owner/{id}")]
        public async Task<ActionResult<List<Animal>>> GetAnimalByOwnerId(int id)
        {
            List<Animal> animals = _animalRepository.GetAnimalByOwnerId(id);
            if (animals == null)
            {
                return NotFound();
            } else if (animals.Count == 0){
                return Problem($"Could not find an Animal associated with AnimalId {id}"); ;
            }
            else
            {
                return animals;
            }
        }

        // PUT: api/Animals/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAnimal(int id, Animal animal)
        {
            if (id != animal.AnimalId)
            {
                return BadRequest();
            }


            Owner fndOwner = _context.Owners.FirstOrDefault(o => o.OwnerId == animal.OwnerId);
            if (fndOwner == null)
            {
                return Problem($"Could not find an Owner associated with AnimalId {animal.OwnerId}");
            }
            animal.Owner = fndOwner;


                // Update existing animal entry
            try
            {
                _animalRepository.Update(animal);
                _animalRepository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_animalRepository.AnimalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            

            return NoContent();
        }

        // POST: api/Animals
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Animal>> PostAnimal(Animal animal)
        {
            if (_context.Animals == null)
            {
                return Problem("Entity set 'AnimalShelterContext.Animals'  is null.");
            }

            if (animal.OwnerId != null && animal.Owner == null)
            {
                Owner fndOwner = _context.Owners.FirstOrDefault(o => o.OwnerId == animal.OwnerId);
                if (fndOwner == null)
                {
                    return Problem($"Could not find an Owner associated with {animal.OwnerId}");
                }
                animal.Owner = fndOwner;
            }

            _animalRepository.Insert(animal);
            _animalRepository.Save();

            return CreatedAtAction("GetAnimal", new { id = animal.AnimalId }, animal);
        }

        // DELETE: api/Animals/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAnimal(int id)
        {
            if (_context.Animals == null || !_animalRepository.AnimalExists(id))
            {
                return NotFound();
            }

            _animalRepository.Delete(id);
            _animalRepository.Save();

            return NoContent();
        }


    }
}
