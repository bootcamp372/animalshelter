﻿using System;
using System.Collections.Generic;

namespace AnimalShelter.Models;

public partial class Owner
{
    public int OwnerId { get; set; }

    public string Name { get; set; } = null!;

    public string? Address1 { get; set; }

    public string? Address2 { get; set; }

    public string? City { get; set; }

    public string? State { get; set; }

    public string? Zipcode { get; set; }

    public string? Email { get; set; }

    public string Phone { get; set; } = null!;
    public virtual ICollection<Animal> Animals { get; } = new List<Animal>();
}
