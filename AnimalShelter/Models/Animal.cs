﻿using System;
using System.Collections.Generic;

namespace AnimalShelter.Models;

public partial class Animal
{
    public int AnimalId { get; set; }

    public string Species { get; set; } = null!;

    public string Breed { get; set; } = null!;

    public int OwnerId { get; set; }

    public DateTime Dob { get; set; }

    public DateTime CheckInDate { get; set; }

    public DateTime CheckOutDate { get; set; }

    public string Name { get; set; } = null!;

    public virtual Owner? Owner { get; set; } = null!;
}
