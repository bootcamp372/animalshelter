﻿using AnimalShelter.Models;
using Microsoft.EntityFrameworkCore;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using System.Collections.Generic;

namespace AnimalShelter.Repository
{
    public class AnimalRepository : IAnimalRepository
    {
        static AnimalShelterContext _context = new AnimalShelterContext();
        public AnimalRepository()
        {
            _context = new AnimalShelterContext();
        }
        public AnimalRepository(AnimalShelterContext context)
        {
            _context = context;
        }
        public IEnumerable<Animal> GetAll()
        {            
            return _context.Animals.Include(a => a.Owner).ToList();
        }
        public Animal GetById(int AnimalId)
        {
            return _context.Animals.Include(a => a.Owner).ToList().Find(a => a.AnimalId == AnimalId);
        }
        public void Insert(Animal animal)
        {
            _context.Animals.Add(animal);
            _context.Animals.Include(a => a.Owner);
        }
        public void Update(Animal animal)
        {
            _context.Entry(animal).State = EntityState.Modified;
        }
        public void Delete(int AnimalId)
        {
            Animal animal = _context.Animals.Find(AnimalId);
            _context.Animals.Remove(animal);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
        public List<Animal> GetAnimalByOwnerId(int OwnerId)
        {
            List<Animal> animalsByOwner = new List<Animal>();

            foreach (Animal animal in _context.Animals)
            {
                if (animal.OwnerId == OwnerId)
                {
                    animalsByOwner.Add(animal);
                }
            }
            return animalsByOwner;
        }

        public bool AnimalExists(int id)
        {
            return (_context.Animals?.Any(e => e.AnimalId == id)).GetValueOrDefault();
        }
/*
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }*/
    }
}
