﻿using AnimalShelter.Models;
using Microsoft.EntityFrameworkCore;

namespace AnimalShelter.Repository
{
    public class OwnerRepository : IOwnerRepository
    {
        static AnimalShelterContext _context = new AnimalShelterContext();
        public OwnerRepository()
        {
            _context = new AnimalShelterContext();
        }
        public OwnerRepository(AnimalShelterContext context)
        {
            _context = context;
        }
        public IEnumerable<Owner> GetAll()
        {

            /*            Console.WriteLine(_context.Animals.ToList()[0].Owner.Name);*/
            return _context.Owners.Include(o => o.Animals).ToList(); 
        }
        public Owner GetById(int OwnerId)
        {
            return _context.Owners.Include(o => o.Animals).ToList().Find(o => o.OwnerId == OwnerId);
        }
        public void Insert(Owner owner)
        {
            _context.Owners.Add(owner);
/*            _context.Owners.Include(o => o.Animals);*/
            /*            _context.SaveChanges();*/
        }
        public void Update(Owner owner)
        {
            _context.Entry(owner).State = EntityState.Modified;
        }
        public void Delete(int OwnerId)
        {
            Owner owner = _context.Owners.Find(OwnerId);
            _context.Owners.Remove(owner);
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        public bool OwnerExists(int id)
        {
            return (_context.Owners?.Any(e => e.OwnerId == id)).GetValueOrDefault();
        }

/*        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }*/
    }
}
