﻿using AnimalShelter.Models;
using Microsoft.EntityFrameworkCore;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using System.Collections.Generic;

namespace AnimalShelter.Repository
{
    public interface IAnimalRepository
    {
        IEnumerable<Animal> GetAll();
        Animal GetById(int AnimalId);
        void Insert(Animal animal);
        void Update(Animal animal);
        void Delete(int AnimalId);
        void Save();
        List<Animal> GetAnimalByOwnerId(int Owner);
        bool AnimalExists(int id);

    }
}
