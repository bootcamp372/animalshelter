﻿using AnimalShelter.Models;

namespace AnimalShelter.Repository
{
    public interface IOwnerRepository
    {
        IEnumerable<Owner> GetAll();
        Owner GetById(int OwnerId);
        void Insert(Owner owner);
        void Update(Owner owner);
        void Delete(int OwnerId);
        void Save();
        bool OwnerExists(int id);
    }
}
